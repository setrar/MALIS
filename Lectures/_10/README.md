# Lecture 10: Ensembles [Jan 12]

Topics:

Ensembles [slides](content/10_ensembles.pdf)

Required Previous Knowledge:

Gradient Descent (review lecture )
Complementary (Advanced) Resources:

SAT-based Decision Tree Learning for Large Data Sets. Research paper on advanced training algorithms for decision trees, 2021 [link]

Boosting Algorithms as Gradient Descent. Research paper on the analysis of boosting via gradient descent, 1999 [link](https://proceedings.neurips.cc/paper/1999/file/96a93ba89a5b5c6c226e49b88973f46e-Paper.pdf)

XGBoost: A Scalable Tree Boosting System. Research paper on an improved version of gradient boosting trees, 2016 [link]
Last modified: Tuesday, 9 January 2024, 9:45 AM
