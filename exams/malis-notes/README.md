# Machine Learning and Intelligent Systems
Summary of the Machine Learning and Intelligent Systems (MALIS) course given by professor Maria A. Zuluaga, EURECOM 2023/2024. 

## Disclaimer
This project is created by students, for students, no responsibility is accepted in case of errors (which you are kindly requested to at least make known, you would be doing a big favor). If you see that there is content missing, create open a pull request with the suggested change

## How to contribute

- Fork the respository
- Change the files
- Push to your repository
- Open a Pull Request to this repository

# References

https://gitlab.eurecom.fr/valentin1/malis-notes.git
